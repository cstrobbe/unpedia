'use strict';

//source: "Creating a Table of Contents in JavaScript" by Frédéric Perrin: https://codepen.io/blustemy/pen/GNNZwp/ (with a few adaptations)
class TableOfContents {
    /*
        The parameters from and to must be Element objects in the DOM.
    */
    constructor({ from, to, h2 }) { // h2 added by CS
        this.fromElement = from;
        this.toElement = to;
        this.tocHeader = h2;
        // Get all the ordered headings.
        this.headingElements = this.fromElement.querySelectorAll("h1, h2, h3, h4, h5, h6");
        this.tocElement = document.createElement("div");
        this.tocHeader = document.createElement("h2"); // added by CS
    }

    /*
        Get the most important heading level.
        For example if the article has only <h2>, <h3> and <h4> tags
        this method will return 2.
    */
    getMostImportantHeadingLevel() {
        let mostImportantHeadingLevel = 6; // <h6> heading level
        for (let i = 0; i < this.headingElements.length; i++) {
            let headingLevel = TableOfContents.getHeadingLevel(this.headingElements[i]);
            mostImportantHeadingLevel = (headingLevel < mostImportantHeadingLevel) ?
                headingLevel : mostImportantHeadingLevel;
        }
        return mostImportantHeadingLevel;
    }

    /*
        Generate a unique id string for the heading from its text content.
    */
    static generateId(headingElement) {
        return headingElement.textContent.replace(/\s+/g, "_");
    }

    /*
        Convert <h1> to 1 … <h6> to 6.
    */
    static getHeadingLevel(headingElement) {
        switch (headingElement.tagName.toLowerCase()) {
            case "h1": return 1;
            case "h2": return 2;
            case "h3": return 3;
            case "h4": return 4;
            case "h5": return 5;
            case "h6": return 6;
            default: return 1;
        }
    }

    generateToc() {
        let currentLevel = this.getMostImportantHeadingLevel() - 1,
            currentElement = this.tocElement;

        // next six lines added by CS
        let ToCContainer = document.getElementById("toc");
        let ToCHeading = document.createElement("h2");
        ToCHeading.appendChild(document.createTextNode("Table of Contents"));
        ToCHeading.setAttribute("id", "toc-heading");
        ToCContainer.appendChild(ToCHeading);
        ToCContainer.classList.remove("hidden"); //@@move class=hidden to nav?

        for (let i = 0; i < this.headingElements.length; i++) {
            let headingElement = this.headingElements[i],
                headingLevel = TableOfContents.getHeadingLevel(headingElement),
                headingLevelDifference = headingLevel - currentLevel,
                linkElement = document.createElement("a");

            if (!headingElement.id) {
                headingElement.id = TableOfContents.generateId(headingElement);
            }
            linkElement.href = `#${headingElement.id}`;
            linkElement.textContent = headingElement.textContent;

            if (headingLevelDifference > 0) {
                // Go down the DOM by adding list elements.
                for (let j = 0; j < headingLevelDifference; j++) {
                    let listElement = document.createElement("ul"),
                        listItemElement = document.createElement("li");
                    listElement.appendChild(listItemElement);
                    currentElement.appendChild(listElement);
                    currentElement = listItemElement;
                }
                currentElement.appendChild(linkElement);
            } else {
                // Go up the DOM.
                for (let j = 0; j < -headingLevelDifference; j++) {
                    currentElement = currentElement.parentNode.parentNode;
                }
                let listItemElement = document.createElement("li");
                listItemElement.appendChild(linkElement);
                currentElement.parentNode.appendChild(listItemElement);
                currentElement = listItemElement;
            }

            currentLevel = headingLevel;
        }

        this.toElement.appendChild(this.tocElement.firstChild);
    }
}

document.addEventListener("DOMContentLoaded", () =>
    new TableOfContents({
        from: document.querySelector(".main"),
        to: document.querySelector(".table-of-contents"),
        h2: document.querySelector("#toc-heading") // added by CS
    }).generateToc()
);

//////////// older attempt: ////////////

//let DEBUG = true;
//let all_headings = Array.from(document.querySelectorAll("h2,h3,h4,h5,h6")); /* https://stackoverflow.com/questions/7065562/how-do-i-get-all-h1-h2-h3-etc-elements-in-javascript */
//if (DEBUG) console.log(all_headings);

/*
function createHeadingsUlList(headingsArray, currentLevel) {
  let ulList = document.createElement("ul");
  ulList.setAttribute("class", "pagenavigation");
  for (let index = 0; index < headingsArray.length; index++) {
    let liNode = document.createElement("li");
    if (headingsArray[index].localName.localeCompare(currentLevel) > 0) { // if localName is sorted after currentLevel, localCompare returns 1
      liNode.appendChild(createHeadingsUlList(headingsArray.slice(headingsArray[index + 1]), headingsArray[index].localName));
      if (DEBUG) console.log("recursive call for sublist: " + "Heading: " + all_headings[index].localName + ": " + all_headings[index].innerText + " _ ID:" + all_headings[index].id);
    } else {
      let linkNode = document.createElement("a");
      let txtNode = document.createTextNode(headingsArray[index].localName + ": " + headingsArray[index].innerText);
      linkNode.setAttribute("href", "#" + headingsArray[index].id);
      linkNode.appendChild(txtNode);
      liNode.appendChild(linkNode);
      ulList.appendChild(liNode);
      //@@todo continue
    }
  }
  return ulList;
}
*/

// Create ul list of headings below h1:
//document.getElementsByTagName("header")[0].insertAdjacentElement('afterend', createHeadingsUlList(all_headings, "h2"));

/*
for (let index = 0; index < all_headings.length; index++) {
  //if (DEBUG) console.log("Heading: " + all_headings[index].localName + ": " + all_headings[index].innerText + " _ ID:" + all_headings[index].id );
}
//let all_h2_headings = document.getElementsByTagName("h2");
//console.log(all_h2_headings);
*/

/*
Using JavaScript to generate a table of contents:
 * Generating A Table Of Contents with Pure JavaScript – TOC. 12.07.2018: https://www.cssscript.com/generating-a-table-of-contents-with-pure-javascript-toc/
   "TOC is a jQuery-free JavaScript plugin that automatically generates a table of contents from HTML heading tags of your long content." MIT licence.
   Code on GitHub: https://github.com/idiotWu/jQuery-TOC
 * Generate A Hierarchical Table Of Contents With Vanilla JS – toc-plugin. 05.09.2018: https://www.cssscript.com/hierarchical-table-of-contents/
   "A vanilla JavaScript TOC plugin used to generate a list based hierarchical table of contents for long web content."
   Licence: MIT? GitHub: https://github.com/tyler-vs/toc-plugin
 * Frédéric Perrin: Creating a Table of Contents in JavaScript, 24.11.2016: https://www.blustemy.io/creating-a-table-of-contents-in-javascript/ [no longer online in August 2022]
   See demo on CodePen: https://codepen.io/blustemy/pen/GNNZwp/

 * Chris Coyier: Automatic Table of Contents. CSS-Tricks, 20.03.2013: https://css-tricks.com/automatic-table-of-contents/
   This relies on jQuery. Nested list?
 * Packt: Using jQuery Script for Creating Dynamic Table of Contents, 21.10.2009: https://hub.packtpub.com/using-jquery-script-creating-dynamic-table-contents/
 * Create Table Of Contents (TOC) With Multi-level List In JavaScript, February 2017: https://www.mybloggertricks.com/2017/02/create-table-of-contents-javascript.html 
   Only for Blogger users.
 * ppk: Table of Contents script. (no date; probably old; only h3 and h4): https://quirksmode.org/dom/toc.html
 * Stuart Langridge: generated-toc: Generate a Table of Contents with the DOM. July 2007, February 2008: https://kryogenix.org/code/browser/generated-toc/
*/