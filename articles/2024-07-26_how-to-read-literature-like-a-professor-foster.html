<!DOCTYPE html><html lang="en-GB" xml:lang="en-GB" xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
  <title>How to Read Literature Like a Professor by Thomas C. Foster | Unpedia</title>
  <meta name="viewport" content="width=device-width" />
  <link rel="stylesheet" type="text/css" media="screen" href="../css/stuttgartbib.css" title="default" />
  <!--
  <link rel="alternate stylesheet" type="text/css" media="screen" href="../css/s_124085_triad.css" title="Three-colour scheme" />
  <link rel="alternate stylesheet" type="text/css" media="screen" href="../css/systemcolours.css" title="System colours" />
  <link rel="alternate stylesheet" type="text/css" media="screen" href="../css/default.css" title="No colours" />
  -->
  <link rel="top" href="../index.html" title="Unpedia home page" />
  <link rel="up" href="./index.html" title="Articles and Reviews" />
  <meta name="robots" content="noindex, nofollow" />
  <meta name="googlebot" content="noindex, nofollow" />
</head>

<body>
  <header>
    <h1><span class="h1txt"><i class="booktitle">How to Read Literature Like a Professor</i> by Thomas C. Foster</span></h1>
  </header>
  <main>
    <p>Novels, short stories, poems and plays all use a large set of conventions that are specific to the genre they belong to. In addition, there is a large set of conventions that work across these genres.
      Learning to recognise these conventions requires practice. This is one of the main messages of Thomas C. Foster's book <i class="booktitle">How to Read Literature Like a Professor</i>.
    </p>
    <p>Foster is a professor at the University of Michigan–Flint and has taught literature for more than three decades.
      He wrote his book mainly for adult returning students, who tend to ask more questions than traditional students about how literary interpretation works.
      To his surprise, Foster later noticed that his book was also picked up by teachers and used in secondary-school classrooms (more specifically, &ldquo;high school&rdquo; in American English).
      He even got invited to talks where secondary-school pupils asked him to sign copies of his book.
    </p>
    <p>The sorts of conventions that Foster's book focuses on are those that allow you to read between the lines: story patterns (for example, the quest),
      allusions and symbols. Due to the diversification of the canon, authors can no longer assume a common body of literature that their readers are familiar with (chapter 7).
      The sources of allusions that Foster discusses are Shakespeare, the Bible, &ldquo;myth&rdquo; in a broad sense (&ldquo;myth is a body of stories that matter&rdquo;) and children's literature (chapters 5–8).
    </p>
    <p>A large part of the book is devoted to explaining many types of symbolism: meals (&ldquo;whenever people eat or drink together, it's communion&rdquo;, chapter 2),
      the different guises of vampirism (&ldquo;ghosts and vampires are never only about ghosts and vampires&rdquo;, chapter 3), weather phenomena such as rain and snow (chapter 9),
      violence (chapters 10–11), flight (&ldquo;flight is freedom&rdquo;, chapter 15), symbols for or coded references to sex (chapter 16), sex itself as a symbol (chapter 17), symbols for baptism (chapter 18), geography as a symbol
      (&ldquo;when writers send characters south, it's so they can run amok&rdquo;, chapter 19), the seasons (chapter 20), deformities and other physical features (chapter 21–22), and heart disease and other illnesses (though not all diseases are created equal, chapter 23).
    </p>
    <p>In Chapter 12, which is titled &ldquo;Is That a Symbol?&rdquo; he discusses a few common problems with the detection and interpretation of <a href="https://literature.stackexchange.com/questions/tagged/symbolism">symbols</a>.
      The first problem is that many readers expect a symbol to mean one particular thing, and one thing only. It is true that some symbols, such as a white flag, have a limited range of meanings,
      but unless you are dealing with allegory, there are usually several valid interpretations.
      Another problem with symbols is that many readers only look at objects and images as potential symbols and ignore events and actions.
      Foster mentions Robert Frost as a poet who often used action as a symbol. I would have appreciated it if the author had expanded on this type of symbols by discussing several examples.
      In a later chapter, Foster also explains the difference between widely understood symbols and symbols that have a specific meaning within a particular author's work.
    </p>
    <p>Foster also points out (in bold) that &ldquo;<strong>characters are not people</strong>&rdquo; but &ldquo;products of writers' imagination—and readers' imagination&rdquo; (chapter 8).
      Terry Eagleton made the same point in the first chapter of his book <i class="booktitle">How to Read Literature</i> (2013), but it bears repeating.
      On forums and question-and-answer sites about literature, people often ask questions that imply that characters should behave like real people,
      even in works of literature dating from periods when real human behaviour was not the main source of inspiration for characters' behaviour.
      (See, for example, some of the <a href="https://literature.stackexchange.com/questions/tagged/character-analysis">questions tagged 'character-analysis'</a> on Literature Stack Exchange.)
    </p>
    <p>One of the last chapters reproduces Katherine Mansfield's well-known short story <a href="https://en.wikipedia.org/wiki/The_Garden_Party_(short_story)">The Garden Party</a>
      as a test case for what you have learnt from the book. Foster asks you to answer a few questions about that story and to write down your observations.
      After that, he gives three examples of readings of the story, which go from a straightforward response to more sophisticated observations.
    </p>
    <p>In spite of the book's title, Foster does not look at literature as a whole but at English literature, especially prose fiction (with a few examples of plays and poems).
      There are few references to literature in other languages, even when including the Bible and Greek mythology, which are discussed because they are important sources of allusions and references.
      The author admits that a single book cannot cover everything (even when focusing only on English prose fiction).
      For example, form and structure are not discussed at all. The first edition contained a chapter on the structure of the sonnet but it was removed from the second edition because it did not really fit in with the rest of the book.
      (In 2018, Foster published <i class="booktitle">How to Read Poetry Like a Professor</i>, in which a chapter on the sonnet makes more sense.)
      He also points out that his book does not cover all the cultural codes that authors use, as that would have required an encyclopaedic approach and would have made his book much less enjoyable.
    </p>
    <p>One of the distinguishing features of the book is how Foster encourages readers to trust their own judgement, especially in the Postlude (titled &ldquo;Who's in Charge Here?&rdquo;)
      and the Envoi. Foster tells readers,</p>
    <blockquote>
      <p>Don't cede control of your opinions to critics, teachers, famous writers, or know-it-all professors. Listen to them, but read confidently and assertively,
        and don't be ashamed or apologetic about your reading. You and I both know you're capable and intelligent, so don't let anyone tell you otherwise.</p>
    </blockquote>
    <p>The book's appendix contains a reading list in several parts: several pages of primary works (all English literature except for Kafka and Sophocles),
      a short list of fairy tales, a list of films, secondary sources (<abbr>i.e.</abbr> other books that can help you become a better reader of literature) and
      a section titled &ldquo;Master Class&rdquo; recommending four works of literature that &ldquo;will give you a chance to use all your newfound skills&rdquo;.
    </p>
    <p>Foster's book is a very enjoyable read and a great place to start for those who would like to study literature, or at least English prose. I highly recommend it.
    </p>
    <p>Thomas C. Foster: <a href="https://www.harpercollins.com/products/how-to-read-literature-like-a-professor-revised-edition-thomas-c-foster?variant=32116793049122"><i class="booktitle">How to Read Literature Like a Professor</i></a>. Revised edition. HarperCollins, 2014. <abbr>ISBN</abbr> 9780062301673.
      (A teaching guide can be downloaded from the HarperCollins website. This <abbr>PDF</abbr> file does not follow <a href="https://www.section508.gov/create/pdfs/">guidelines for the creation of accessible <abbr>PDF</abbr> documents</a>.)
    </p>
    <hr />
    <p>Review also published on
      <a href="https://oneminutereviews.tumblr.com/post/757064923341799424/how-to-read-literature-like-a-professor-by-thomas">One Minute Reviews</a> on 26.07.2024.
    </p>
  </main>
  <footer>
    <p id="licence">
    <a class="image" rel="license" href="http://creativecommons.org/licenses/by-nc-nd/4.0/"><img alt="Creative Commons License" style="border-width:0" src="../img/cc_by-nc-nd-4.0_88x31.png" /></a><br />
    <strong>Unpedia</strong> by <a href="https://gitlab.com/cstrobbe">Christophe Strobbe</a> is licensed under a
    <a lang="en-US" rel="license" href="http://creativecommons.org/licenses/by-nc-nd/4.0/">Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License</a>.
    </p>
  </footer>
</body></html>
